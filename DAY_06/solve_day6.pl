#!/usr/bin/perl

# ------------------------------------------------------------------------------
# Copyright 2022, Damiano Verzulli - damiano.verzulli@garrlab.it
#
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU Affero General Public License as published by the 
# Free Software Foundation, either version 3 of the License, or (at your 
# option) any later version.
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
# for more details.
# You should have received a copy of the GNU Affero General Public License 
# along with this program. If not, see <https://www.gnu.org/licenses/>. 
# ------------------------------------------------------------------------------

use Data::Dumper;
use POSIX;

my $runInP2 = 0;
if ($ARGV[0] eq 'P2') {
  print "We are on P2! Let's act accordingly!\n";
  $runInP2 = 1;
}

open(FH, "./input.txt") || die ("Unable to open input.txt: $!");
my $timeString = <FH>;
my $distanceString = <FH>;
close FH;

# Let's strip final newline
chomp $timeString;
chomp $distanceString;

my @times = $timeString =~ /\d+/g;
my @distances = $distanceString =~ /\d+/g;

if ($runInP2) {

  # NO: 20048740
  # NO: 14860245
  my $T = join('',@times);
  my $D = join('',@distances);

  printf("We're on P2 => Time: [%d] - Distance [%d]\n",$T, $D);

  # We've **GIANT** numbers, so... we need to work differently. We need a 2nd grade equation:
  #    t1 * (T-t1) > D
  # to be rewritten in:
  #    t1^2 - t1*T + D > 0
  # -> t1 = [T +/- SQRT(T^2 - 4D)] / 2

  # luckily, my PERL seems to support such a *BIG* number!
  my $sqarg = $T**2 - 4*$D;
  my $sq = sqrt($sqarg);
  my $t1 = ($T - $sq ) / 2;
  my $t2 = ($T + $sq ) / 2;

  printf("Square arg: [%d] - Square root: [%f]\n", $sqarg, $sq);
  printf("zeros are in => t1:[%f] - t2:[%f]\n",$t1, $t2);
  
  my $t1Int = ceil($t1);
  my $t2Int = floor($t2);

  printf("our zeros are in => t1:[%d] - t2:[%d]\n",$t1Int, $t2Int);
  printf("So we can win [%d] races...\n", $t2Int - $t1Int + 1);

} else {

  # to store the final product
  my $productStore = 1;

  print "check run:\n";
  foreach my $idx (0..$#times) {
    printf("Round [%d] => time: [%d] distance [%d]\n", $idx, $times[$idx], $distances[$idx]);

    # to calculate the space S traveled in T time, where t1 time (t1 < T) is spent to "charge", the formula is:
    # S = t1 * (T - t1)
    # where t1 can be any integer from 0 to T
    # 
    # ...and we'll "win" if S > D (where D is the "distance" mentioned in input)

    # Let's calculate inputs
    my $T = $times[$idx];
    my $D = $distances[$idx];

    # let's track our wins
    $winCounter = 0;

    # So let's loop over T
    foreach my $t1 (1..$T) {
      my $S = $t1 * ($T - $t1);

      # do we won?
      if ($S > $D) {
        # yes!
        $winCounter++;
      };
    }

    # Let's update the product store
    # (note: winCounter = 0 *IS* a proper value!)
    $productStore = $productStore * $winCounter;
  }

  print "Final product is: [" . $productStore . "]\n";

}

