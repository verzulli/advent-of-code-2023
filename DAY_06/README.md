TL/DR;
======

Original text
=============

*to be written. Sorry!*

My notes
========

This is a **VERY** simple challenge. I simply wrote down the formula `S = t1 * (T - t1)` where:

   * `S` is the total distance covered by us
   * `T` is the duration of the run;
   * `t1` is the number of milliseconds (integer) spent in "charging". Of course, `0 <= t1 <= T`

and
   * `D` is the distance we need to exceed

The soultion is **VERY** simple and self-explanatory.

Unfortunately this approach **CANNOT** be followed for P2 has it would require a HUGE time, for looping (our `T` is `34908986`)-

So I opted for a DISEQUATION. As reported in the code:

      We've **GIANT** numbers, so... we need to work differently. 
      We need a 2nd grade equation:

         t1 * (T-t1) > D
      
      to be rewritten in:

         t1^2 - t1*T + D > 0

      and then as for t1:
      
         t1 = [T +/- SQRT(T^2 - 4D)] / 2

So, the code simply calculate the two values for t1 and count the related values in the middle.

A final note: I opted to have a **single** code. To run P2, it simply requires a "P2" param, at the end.

Solutions
=========
As for my solution:

* My input data are in the "input.txt"
* My implemented solution is [solve_day6.pl](solve_day6.pl)
* P2 result can be obtained by adding `P2` as argument, at the end of the command

Results
=======

P1
--

      [verzulli@XPSGarr DAY_06]$ perl solve_day6.pl 
      check run:
      Round [0] => time: [34] distance [204]
      Round [1] => time: [90] distance [1713]
      Round [2] => time: [89] distance [1210]
      Round [3] => time: [86] distance [1780]
      Final product is: [633080]
      [verzulli@XPSGarr DAY_06]$ 

P2
--

      [verzulli@XPSGarr DAY_06]$ perl solve_day6.pl P2
      We are on P2! Let's act accordingly!
      We're on P2 => Time: [34908986] - Distance [204171312101780]
      Square arg: [401952055141076] - Square root: [20048741.984002]
      zeros are in => t1:[7430122.007999] - t2:[27478863.992001]
      our zeros are in => t1:[7430123] - t2:[27478863]
      So we can win [20048741] races...
      [verzulli@XPSGarr DAY_06]$