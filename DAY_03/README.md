TL/DR;
======
Given a matrix of chars, you need to find "symbols" (chars different from "." and digit) and do some calculation on surrouding numbers

Original text
=============

    [...]
    The engine schematic (your puzzle input) consists of a visual representation of the engine. There are lots of numbers and symbols you don't really understand, but apparently any number adjacent to a symbol, even diagonally, is a "part number" and should be included in your sum. (Periods (.) do not count as a symbol.)

    Here is an example engine schematic:

    467..114..
    ...*......
    ..35..633.
    ......#...
    617*......
    .....+.58.
    ..592.....
    ......755.
    ...$.*....
    .664.598..

    In this schematic, two numbers are not part numbers because they are not adjacent to a symbol: 114 (top right) and 58 (middle right). Every other number is adjacent to a symbol and so is a part number; their sum is 4361.

    Of course, the actual engine schematic is much larger. What is the sum of all of the part numbers in the engine schematic?
    [...]

My notes
========
My PERL implementation has revealed trickier than I expected: P1 took me 01:27:01 and P2 got even trickier, as it took 14:46:36.

The trickiest point was due to the fact that "surrounding numbers" could span **OVER** the adjacent box of the symbols... and they could span left and/or right... this need to be taken into account, as otherwise we run the risk to count the same number, twice...

As for P2, thing got worse as... I haven't kept track of "surrouding numbers" while I discovered them... So --**as time were running out!!!**-- I had to rewrite the whole routine and redo the calculation... And it work!

* My input data are in the "input.txt"
* My implemented solution is "solve_day3.pl"

Solving approach
----------------
Due to time issue, I had to accelerate the development and... so I opted for variour "dirty" code. Expecially for P2. **I don't expect it could result readable for novice PERL users**. Sorry!

P2
--
P2 is even simpler.

    --- Part Two ---

    A gear is any * symbol that is adjacent to exactly two part numbers. Its gear ratio is the result of multiplying those two numbers together.

    This time, you need to find the gear ratio of every gear and add them all up so that the engineer can figure out which gear needs to be replaced.

    Consider the same engine schematic again:

    467..114..
    ...*......
    ..35..633.
    ......#...
    617*......
    .....+.58.
    ..592.....
    ......755.
    ...$.*....
    .664.598..

    In this schematic, there are two gears. The first is in the top left; it has part numbers 467 and 35, so its gear ratio is 16345. The second gear is in the lower right; its gear ratio is 451490. (The * adjacent to 617 is not a gear because it is only adjacent to one part number.) Adding up all of the gear ratios produces 467835.

    What is the sum of all of the gear ratios in your engine schematic?


Implementation
==============

* [`solve_day3.pl`](./solve_day3.pl) for both P1 and P2

Results
=======

    [verzulli@XPSGarr DAY_03]$ perl solve_day3.pl  
    Numero totale righe: [140]
    ----------------
    Somma = [538046]
    P2 total = [81709807]
    [verzulli@XPSGarr DAY_03]$ 
