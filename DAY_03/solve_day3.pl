#!/usr/bin/perl

# ------------------------------------------------------------------------------
# Copyright 2022, Damiano Verzulli - damiano.verzulli@garrlab.it
#
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU Affero General Public License as published by the 
# Free Software Foundation, either version 3 of the License, or (at your 
# option) any later version.
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
# for more details.
# You should have received a copy of the GNU Affero General Public License 
# along with this program. If not, see <https://www.gnu.org/licenses/>. 
# ------------------------------------------------------------------------------

use Data::Dumper;

sub lookAround($$$);
sub extract($$);
sub checkP2();
sub calculateP2();

open(FH, "./input.txt") || die ("Unable to open input.txt: $!");
my @lines = <FH>;
close FH;

my $totline = @lines;
print "Numero totale righe: [" . $totline . "]\n";


my $extractMap = {};
my $P2Store = {};

my $row=0;
# Let's read the file, line by line
foreach my $line (@lines) {
  chomp $line;
  $row++;

  my $l = length($line);

  # for each line, loop over all columns...
  foreach my $col (1..$l) {

    # ...cheching for a "symbol"
    my $s = substr($line,$col-1,1);
    if ($s !~ /[\d\.]/) {

      # found! Let's find what's around here ()...
      # printf("[%d/%d] %s\n",$row,$col,$s);
      lookAround($line, $row,$col);

    }
  }
}

# Let's initialize the temporary SUM
my $sum=0;

print "----------------\n";
foreach my $mRiga (sort keys %{$extractMap}) {
  foreach my $mCol (reverse sort keys %{$extractMap->{$mRiga}}) {
    if (defined $extractMap->{$mRiga}{$mCol-1}) {
      # skippo
      # printf("Extract: [r:%d] [c:%d] *\n",$mRiga,$mCol);
    } else {
      # prendo
      # printf("Extract: [r:%d] [c:%d]\n",$mRiga,$mCol);
      my $v = extract($mRiga,$mCol);

      $sum += $v;
    }
  }
}

print "Somma = [" . $sum . "]\n";

my $P2Map = {};
checkP2();
calculateP2();

exit;

sub lookAround($$$) {
  my ($l, $r, $c) = @_;

  # loop over "previous column", "this column", "next column"
  foreach my $x ($c-1 .. $c+1) {

    # take care to not cross left and right borders
    next if ($x==0);
    next if ($x>length($l));

    # loop over "previous row", "this row", "next row"
    foreach my $y ($r-1 .. $r+1) {

      # take care to not cross top and bottom borders
      next if ($y==0);
      next if ($y>$totline);

      # no need to check "my" position (as I'm at the center, and I know to be a symbol)
      next if ($x == $c && $y == $r);

      # take the whole line we're checking
      my $tline = $lines[$y-1];
      chomp $tline;

      # if we've a digit, here, we need to EXTRACT the whole of it. So let's track it
      # in $extraMap hash
      my $ch = substr($tline,$x-1, 1);
      if ($ch =~ /\d/) {

        # Track this point/symbol, for P2
        my $P2Key = sprintf("%d/%d",$r, $c);
        $P2Store->{$P2Key}=1;

        $extractMap->{$y}{$x}++;
      }
      # printf("%s: (%d/%d) [%s]\n", $tline, $y, $x, $ch);
    }
    # print "\n";
  }
  # print "\n";
}

sub extract ($$) {
  my ($r, $c) = @_;

  my $l = $lines[$r-1];
  chomp $l;

  my @tmp;
  push (@tmp,substr($l,$c-1,1));
  my $ptr = $c-2;
  while ($ptr >= 0) {
    if (substr($l,$ptr,1) =~ /\d/) {
      unshift(@tmp, substr($l,$ptr,1));
      $ptr--;
    } else {
      last;
    }
  }
  foreach $ptr ($c .. length($l)) {
    if (substr($l,$ptr,1) =~ /\d/) {
      push(@tmp, substr($l,$ptr,1))
    } else {
      last;
    }
  }

  my $result = join('',@tmp);
  # print "Beccato: [" . $result. "]\n";
  return $result;
}

sub checkP2() {
  foreach my $tmp (keys %{$P2Store}) {
    my ($r, $c) = split('/', $tmp);
    # printf("[%s] Checcko: r:%d c:%d\n", $tmp, $r, $c);

    # loop over "previous row", "this row", "next row"
    foreach my $y ($r-1 .. $r+1) {

      # take care to not cross top and bottom borders
      next if ($y==0);
      next if ($y>$totline);

      # take the whole line we're checking
      my $tline = $lines[$y-1];
      chomp $tline;

      # loop over "previous column", "this column", "next column"
      foreach my $x ($c-1 .. $c+1) {

        # take care to not cross left and right borders
        next if ($x==0);
        next if ($x>length($tline));

        # no need to check "my" position (as I'm at the center, and I know to be a symbol)
        next if ($x == $c && $y == $r);

        # if we've a digit, here, we need to EXTRACT the whole of it. So let's track it
        # in $extraMap hash
        my $ch = substr($tline,$x-1, 1);
        if ($ch =~ /\d/) {
          my $e = extract($y, $x);
          my $skey = sprintf("%d/%d",$r,$c);
          if (!defined($P2Map->{$skey})) {
            $P2Map->{$skey}={};
          }
          if (!defined($P2Map->{$skey}->{$e})) {
            $P2Map->{$skey}->{$e}=0;
          }
          $P2Map->{$skey}->{$e}++;
        }
      }
    }
  }
}

sub calculateP2() {
  my $sum = 0;
  foreach my $tmp (keys %{$P2Map}) {
    my ($r, $c) = split('/', $tmp);
    # printf("[%s] Checcko: r:%d c:%d\n", $tmp, $r, $c);
    my @num = keys %{$P2Map->{$tmp}};
    if (scalar @num == 2) {
      my $prod = $num[0] * $num[1];
      # printf("\t [%d]->[%s] => [%d]\n\n", scalar @num, join ('|',@num), $prod);
      $sum += $prod;
    }
  }
  print "P2 total = [" . $sum . "]\n";
}