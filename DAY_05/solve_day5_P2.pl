#!/usr/bin/perl

# ------------------------------------------------------------------------------
# Copyright 2022, Damiano Verzulli - damiano.verzulli@garrlab.it
#
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU Affero General Public License as published by the 
# Free Software Foundation, either version 3 of the License, or (at your 
# option) any later version.
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
# for more details.
# You should have received a copy of the GNU Affero General Public License 
# along with this program. If not, see <https://www.gnu.org/licenses/>. 
# ------------------------------------------------------------------------------

use strict;
use Data::Dumper;

# Given a seed, find its location
sub createNextCardMap();


# Open the source file
open(FH, "./input.txt") || die ("Unable to open input.txt: $!");
# open(FH, "./input.txt") || die ("Unable to open input.txt: $!");

# get seeds line:
my $seedsString = <FH>;
chomp $seedsString;

# ...and extract all seed numbers
my @seeds = $seedsString =~ /\d+/g;

### P2
my @seedRanges = ();
while (my $from = shift(@seeds)) {
  my $size = shift(@seeds);
  my $ele = {
    'from' => $from,
    'to' => $from+$size-1
  };
  push(@seedRanges,$ele);
}

# throw away next line
my $dummy = <FH>;

my $currentMinimum;

# Let's loop over "block-MAPS", by loading them in the Hash-ref $cardMapRefs
while (my $cardMapRefs = createNextCardMap()) {
  
  # dumping ranges, for debugging
  printf("============================================\n");
  printf("Current ranges:\n");
  my @deb = sort { $a->{'from'} <=> $b->{'from'} } @seedRanges;
  foreach my $r (@deb) {
    printf("[%d-%d]",$r->{'from'}, $r->{'to'});
  };
  printf("\n---\n");

  # As we're going to SPLIT the ranges, we're going to STORE new ranges
  # in this @nextTargetRanges.
  # So let's initialize it...
  my @nextTargetRanges = ();

  # Let's loop over all of the INPUT ranges (our card ranges)...
  # Please note that @seedRanges is DYNAMIC, as we're going to SPLIT some
  # of the ranges, in sub-ranges, as they can OVERLAP multiple maps so...
  # in such a case, we "split" the sub-ranges + treat single element and 
  # push back the other, for further processing
  while (my $range  = shift(@seedRanges)) {
    
    printf("Going to check RANGE: [%d-%d]\n",$range->{'from'}, $range->{'to'});
    
    # we need to know if some "match" has been found. So let's track, somehow
    my $rangeProcessed = 0;

    # Loop over maps to find DEST
    foreach my $cardRef (@{$cardMapRefs}) {
      printf("\tcheck with MAP [%d-%d] offset [%d]: ", $cardRef->{'source_start'}, $cardRef->{'source_end'}, $cardRef->{'offset'});

      if (
        # If it's a range fully-included...
        $range->{'from'} >= $cardRef->{'source_start'} &&
        $range->{'to'} <= $cardRef->{'source_end'}
      ) {
        # ...our re-mapping is easy: just apply "offset"
        my $tmp = {
          'from' => $range->{'from'} + $cardRef->{'offset'},
          'to' => $range->{'to'} + $cardRef->{'offset'}
        };
        push(@nextTargetRanges, $tmp);
        $rangeProcessed = 1;
        print "+0 - Fully included - [" . scalar @seedRanges . "] ranges...\n";
        # ---------------------------------------------

      } elsif (
        # If it's fully external...
        $range->{'to'} < $cardRef->{'source_start'} ||
        $range->{'from'} > $cardRef->{'source_end'}
      ) {
        # ... nothing to do!
        print "+0 - Fully external - [" . scalar @seedRanges . "] ranges...\n";
        # ---------------------------------------------

      } elsif (
        # If it overlaps LEFT...
        $range->{'from'} < $cardRef->{'source_start'} &&
        $range->{'to'} >= $cardRef->{'source_start'} &&
        $range->{'to'} <= $cardRef->{'source_end'}
      ) {
        # We have to split it. So...
        # let's push the "included" subset
        my $tmp = {
          'from' => $cardRef->{'source_start'} + $cardRef->{'offset'},
          'to' => $range->{'to'} + $cardRef->{'offset'}
        };
        push(@nextTargetRanges, $tmp);

        # ...and let's re-push the remaining part to the range set
        my $rest = {
          'from' => $range->{'from'},
          'to' => $cardRef->{'source_start'} - 1
        };
        push(@seedRanges, $rest);
        $rangeProcessed = 1;
        print "+1 Overlap LEFT - [" . scalar @seedRanges . "] ranges...\n";
        # ---------------------------------------------

      } elsif (
        # If it overlaps RIGHT...
        $range->{'from'} >= $cardRef->{'source_start'} &&
        $range->{'from'} <= $cardRef->{'source_end'} &&
        $range->{'to'} > $cardRef->{'source_end'}
      ) {
        # We have to split it. So...
        # let's push the "included" subset
        my $tmp = {
          'from' => $range->{'from'} + $cardRef->{'offset'},
          'to' => $cardRef->{'source_end'} + $cardRef->{'offset'}
        };
        push(@nextTargetRanges, $tmp);

        # ...and let's re-push the remaining part to the range set
        my $rest = {
          'from' => $cardRef->{'source_end'}+1,
          'to' => $range->{'to'}
        };
        push(@seedRanges, $rest);        
        $rangeProcessed = 1;
        print "+1 Overlap RIGHT - [" . scalar @seedRanges . "] ranges...\n";
        # ---------------------------------------------

      } elsif (
        # If it's a fully superset....
        $range->{'from'} < $cardRef->{'source_start'} &&
        $range->{'to'} > $cardRef->{'source_end'}
      ) {
        # ...we need to split it in three parts :-(

        # The "middle" part...
        my $tmp = {
          'from' => $cardRef->{'source_start'} + $cardRef->{'offset'},
          'to' => $cardRef->{'source_end'} + $cardRef->{'offset'}
        };
        push(@nextTargetRanges, $tmp);

        # The "left" part...
        my $left = {
          'from' => $range->{'from'},
          'to' => $cardRef->{'source_start'} - 1
        };
        push(@seedRanges, $left);

        # The "right" part...
        my $right = {
          'from' => $cardRef->{'source_end'} + 1,
          'to' => $range->{'to'}
        };
        push(@seedRanges, $right);
        $rangeProcessed = 1;
        print "+2 Fully superset - [" . scalar @seedRanges . "] ranges...\n";
        # ---------------------------------------------

      } else {
        print "Dont know...\n";
        exit;
      }
    };

    # let's check if some remapping happened in some MAP
    if ($rangeProcessed == 0) {
      push(@nextTargetRanges, $range);
      print "Adding range with NO remapping (no match)\n";
    }
  }

  # Update the ranges to be processed with the new ones
  @seedRanges = @nextTargetRanges;
  print "===> \@seedRanges now has [" . scalar @seedRanges . "] ranges...\n";
}

# dumping ranges, for debugging
printf("============================================\n");
printf("    FINAL RANGES \n");
my @deb = sort { $a->{'from'} <=> $b->{'from'} } @seedRanges;
foreach my $r (@deb) {
  printf("[%d-%d]",$r->{'from'}, $r->{'to'});
};
printf("\n---\n");


exit;

sub createNextCardMap() {

  my $map = [];

  print "Mapping world...";

  my $tmpline=<FH>;
  chomp $tmpline;
  return 0 if ($tmpline =~ /^$/);

  if ($tmpline =~ /map:$/) {
    # it's ok...
    print "...in map [".$tmpline."]\n";
    
    while (my $line = <FH>) {
      chomp $line;
      last if ($line =~ /^$/);
      my ($destStart, $sourceStart, $num) = $line =~ /\d+/g;
      printf("Letti: [%2d] [%2d] [%2d]\n", $destStart, $sourceStart, $num);

      my $tmp = {
        'source_start' => $sourceStart,
        'source_end' => $sourceStart + $num -1,
        'offset' => $destStart - $sourceStart
      };
      push(@{$map}, $tmp);
    };

  } else {
    print "Uhm... Something is wrong. Please check\n"
  }

  # just for our convenience, let's SORT the map, based on "source_start"
  my @out = sort { $a->{'source_start'} <=> $b->{'source_end'} } @{$map};
  return \@out;
}