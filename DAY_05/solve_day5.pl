#!/usr/bin/perl

# ------------------------------------------------------------------------------
# Copyright 2022, Damiano Verzulli - damiano.verzulli@garrlab.it
#
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU Affero General Public License as published by the 
# Free Software Foundation, either version 3 of the License, or (at your 
# option) any later version.
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
# for more details.
# You should have received a copy of the GNU Affero General Public License 
# along with this program. If not, see <https://www.gnu.org/licenses/>. 
# ------------------------------------------------------------------------------

use strict;
use Data::Dumper;

# Given a seed, find its location
sub createNextMap();


# Open the source file
open(FH, "./input.txt") || die ("Unable to open input.txt: $!");
# open(FH, "./input.txt") || die ("Unable to open input.txt: $!");

# get seeds line:
my $seedsString = <FH>;
chomp $seedsString;

# ...and extract all seed numbers
my @seeds = $seedsString =~ /\d+/g;

# throw away next line
my $dummy = <FH>;

my $currentMinimum;

while (my $mapRef = createNextMap()) {
  

  # It's ALWAYS updated, but we're interested in final values only
  $currentMinimum = 99999999999999;

  # Loop over seeds, to calculate "distance"
  my @nextTargets = ();
  foreach my $seed (@seeds) {
    
    my $next = $seed;

    # Loop over maps to find DEST
    foreach my $mapElement (@{$mapRef}) {
      if (
        $seed >= $mapElement->{'source_start'} &&
        $seed <= $mapElement->{'source_end'} 
      ) {
        $next = $seed + $mapElement->{'offset'};
        last;
      }
    }

    printf("Seed [%d]: next [%d]\n", $seed, $next);
    push (@nextTargets, $next);
  }
  print join('|',@nextTargets);

  print "ITERATE...\n";

  @seeds = @nextTargets;
  
  # update minimum
  foreach my $m (@nextTargets) {
    if ($m < $currentMinimum) {
      $currentMinimum = $m;
    }
  }
}

print "\n----\nMinimum: [" . $currentMinimum . "]\n";
exit;

sub createNextMap() {

  my $map = [];

  print "Mapping world...";

  my $tmpline=<FH>;
  chomp $tmpline;
  return 0 if ($tmpline =~ /^$/);

  if ($tmpline =~ /map:$/) {
    # it's ok...
    print "...in map [".$tmpline."]\n";
    
    while (my $line = <FH>) {
      chomp $line;
      last if ($line =~ /^$/);
      my ($destStart, $sourceStart, $num) = $line =~ /\d+/g;
      printf("Letti: [%2d] [%2d] [%2d]\n", $destStart, $sourceStart, $num);

      my $tmp = {
        'source_start' => $sourceStart,
        'source_end' => $sourceStart + $num -1,
        'offset' => $destStart - $sourceStart
      };
      push(@{$map}, $tmp);
    };

  } else {
    print "Uhm... Something is wrong. Please check\n"
  }
  return $map;
}