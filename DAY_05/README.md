TL/DR;
======

With P1, there are no significant problems... but with P2 things can get quickly worse, as *CARD-ranges* can overlap with *MAP-ranges* and... as they can overlap LEFT, overlap RIGHT, be fully included or be fully excluded... tracking SETs properly (by splitting them and offsetting only relevant parts) can be tricky...

In the end, I have to confess that I didn't enjoyed resolving it. It was much more "uselessly complex" than it was "interesting".


Original text
=============

*to be written. Sorry!*

My notes
========

It's worth noting that my:

* while you can implement P1 (and P2, but also for testing data) with a "full-map" (aka, a 1-to-1 mapping from source to dest), this is definitely **NOT** possible with real P2 data as they are... **huge** numbers (and requires LOT of memory and, more important, **LOT** of processing time)  ;

* so, for P2, you **NEED** to deal with "sets", as they are.

Also:

* my implementation for P2 is **BUGGY**, but such bugs don't prevent proper calculation of lower bound.... So... **WON'T FIX** :-)

That's it!

As for my solution: 

* My input data are in the "input.txt"
* My implemented solution is [solve_day5.pl](solve_day5.pl)
* My implemented (and buggy!) solution for P2 solution is [solve_day5_P2.pl](solve_day5_P2.pl)

Results
=======

P2
--
    [verzulli@XPSGarr DAY_05]$ perl solve_day5_P2.pl | head -n 100
    Mapping world......in map [seed-to-soil map:]
    Letti: [3333452986] [2926455387] [455063168]
    Letti: [3222292973] [1807198589] [111160013]
    Letti: [4073195028] [1120843626] [221772268]
    Letti: [3215232741] [2255546991] [7060232]
    Letti: [1658311530] [2727928910] [32644400]
    Letti: [2680271553] [1918358602] [337188389]
    Letti: [1690955930] [3973557555] [28589896]
    Letti: [2081345351] [4046183137] [248784159]
    Letti: [2374165196] [3613106716] [306106357]
    Letti: [1553535599] [2504868379] [49003335]
    Letti: [4018850546] [3919213073] [54344482]
    Letti: [2050713919] [2287201502] [30631432]
    Letti: [3183342019] [1775307867] [31890722]
    Letti: [3975551599] [2553871714] [43298947]
    Letti: [1120843626] [1342615894] [432691973]
    Letti: [2330129510] [4002147451] [44035686]
    Letti: [1719545826] [2628348978] [99579932]
    Letti: [1819125758] [3381518555] [231588161]
    Letti: [1627133213] [2597170661] [31178317]
    Letti: [3017459942] [2760573310] [165882077]
    Letti: [3788516154] [2317832934] [187035445]
    Letti: [1602538934] [2262607223] [24594279]
    ============================================
    Current ranges:
    [35205517-287303262][289354458-453860630][1117825174-1397139607][1415635989-1581723283][1652880954-1993826501][2160384960-2309873594][2637152665-2873944599][3227452369-3373092395][3561206012-4044566463][4088478806-4203284202]
    ---
    Going to check RANGE: [4088478806-4203284202]
        check with MAP [1120843626-1342615893] offset [2952351402]: +0 - Fully external - [9] ranges...
        check with MAP [1342615894-1775307866] offset [-221772268]: +0 - Fully external - [9] ranges...
        check with MAP [1775307867-1807198588] offset [1408034152]: +0 - Fully external - [9] ranges...
        check with MAP [1807198589-1918358601] offset [1415094384]: +0 - Fully external - [9] ranges...
        check with MAP [1918358602-2255546990] offset [761912951]: +0 - Fully external - [9] ranges...
        check with MAP [2255546991-2262607222] offset [959685750]: +0 - Fully external - [9] ranges...
        check with MAP [2262607223-2287201501] offset [-660068289]: +0 - Fully external - [9] ranges...
    [...]
        check with MAP [3977684907-4107201206] offset [-1925055541]: +0 - Fully external - [0] ranges...
        check with MAP [4107201207-4294967295] offset [-1404363283]: +0 - Fully external - [0] ranges...
    ===> @seedRanges now has [23393] ranges...
    Mapping world...============================================
        FINAL RANGES 
    [17729182-24632190][17729182-24632190][17729182-24632190][17729182-24632190]

where you can see **17729182** as my minimum....