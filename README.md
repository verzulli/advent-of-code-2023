What's this?
============

This project contains my notes and solutions for the [Advent of Code 2023](https://adventofcode.com/2023/about) challenges.

As you probably know, they are 25 programming challenges that you should solve based on your programming skills...

I just published this very project to keep track of my progress

Each single challenge is summarized and implemented in related folder.

* [Day 1: Trebuchet?! - **solved!**](DAY_01/README.md)
* [Day 2: Cube Conundrum - **solved!**](DAY_02/README.md)
* [Day 3: Gear Ratios - **solved!**](DAY_03/README.md)
* [DAY 4: Scratchcards - **solved!**](DAY_04/README.md)
* [DAY 5: If You Give A Seed A Fertilizer - **solved!**](DAY_05/README.md)
* [DAY 6: Wait For It - **solved!**](DAY_06/README.md)
* [DAY 7: Camel Cards - **solved!**](DAY_07/README.md)
* [DAY 8: Haunted Wasteland - **solved!**](DAY_08/README.md)
* [DAY 9: Mirage Maintenance - **solved!**](DAY_09/README.md)
* DAY 10: *to be done*
* DAY 11: *to be done*
* DAY 12: *to be done*
* DAY 13: *to be done*
* DAY 14: *to be done*
* DAY 15: *to be done*
* DAY 16: *to be done*
* DAY 17: *to be done*
* DAY 18: *to be done*
* DAY 19: *to be done*
* DAY 20: *to be done*
* DAY 21: *to be done*
* DAY 22: *to be done*
* DAY 23: *to be done*
* DAY 24: *to be done*
* DAY 25: *to be done*

Why this project?
-----------------

Because I like spending time programming and... have some free time to spend in front of my PC.

...and also, 'cause I really like to "share" my outcomes with people that, maybe, could find it useful! :-)
