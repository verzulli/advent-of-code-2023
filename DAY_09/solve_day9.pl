#!/usr/bin/perl

# ------------------------------------------------------------------------------
# Copyright 2022, Damiano Verzulli - damiano.verzulli@garrlab.it
#
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU Affero General Public License as published by the 
# Free Software Foundation, either version 3 of the License, or (at your 
# option) any later version.
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
# for more details.
# You should have received a copy of the GNU Affero General Public License 
# along with this program. If not, see <https://www.gnu.org/licenses/>. 
# ------------------------------------------------------------------------------

use Data::Dumper;
use strict;

sub addSubSequence($$);
sub isZeroed($$);
sub dumpa($);

my $runInP2 = 0;
if ($ARGV[0] eq 'P2') {
  print "We are on P2! Let's act accordingly!\n";
  $runInP2 = 1;
}

# Let's define a "store" for our main sequences...
my $seqStore = {};

# Let's loop over the input
open(FH, "./input.txt") || die ("Unable to open input.txt: $!");
my $counter = 1;
while (my $line = <FH>) {
  chomp($line);
  my @values = split(/\s+/,$line);

  $seqStore->{$counter}->{1} = \@values;

  $counter++;
}

# Let's start calculating...
foreach my $sKey (sort keys %{$seqStore}) {
  # $sKey is the hash-key for our sequence array

  my $curLevel = 1;
  my $loop = 1;
  while ($loop) {
    my $sequence = $seqStore->{$sKey}->{$curLevel};
    my $seqLen = scalar @{$sequence};
    printf("Working with SEQ [%d] of size [%d]: [%s]\n", $sKey, $seqLen, join('|', @{$sequence}));

    addSubSequence($sKey, $curLevel+1);

    if (! isZeroed($sKey, $curLevel+1)) {
      $curLevel++;
    } else {
      last;
    }   
  }
}

print "PRIMA....\n";
dumpa($seqStore);

# Everything is ready. Let's start "adding" final values
foreach my $sKey (sort keys %{$seqStore}) {
  # $sKey is the hash-key for our sequence array

  my @subIndexes = sort {$b <=> $a} keys %{$seqStore->{$sKey}};
  printf("Working with [%d]: [%s]\n", $sKey, join('|', @subIndexes));

  my $idx = (scalar @subIndexes) - 1;
  while ($idx > 0) {
    # lower array is in: $seqStore->{$sKey}->{$idx+1};
    # upper array is in: $seqStore->{$sKey}->{$idx};
    my $lowerRef = $seqStore->{$sKey}->{$idx+1};
    my $upperRef = $seqStore->{$sKey}->{$idx};

    if (! $runInP2) {
      #P1
      my $newVal = $$lowerRef[-1] + $$upperRef[-1];

      push (@{$seqStore->{$sKey}->{$idx}}, $newVal);
      printf("\tEND-Added [%d] to [%d/%d]\n", $newVal, $sKey, $idx);
    } else {
      #P2
      my $newVal = $$upperRef[0] - $$lowerRef[0];

      unshift (@{$seqStore->{$sKey}->{$idx}}, $newVal);
      printf("\tSTART-Added [%d] to [%d/%d]\n", $newVal, $sKey, $idx);
    }
    $idx--;
  }
}

print "DOPO....\n";
dumpa($seqStore);

my $sum=0;
foreach my $sKey (sort keys %{$seqStore}) {
  # $sKey is the hash-key for our sequence array

  if (! $runInP2) {
    #P1
    my $last = ${$seqStore->{$sKey}->{1}}[-1];
    printf("\tSerie [%d]: last [%d]\n", $sKey, $last);
    $sum += $last;
  } else {
    # P2
    my $first = ${$seqStore->{$sKey}->{1}}[0];
    printf("\tSerie [%d]: first [%d]\n", $sKey, $first);
    $sum += $first;
  }
}
print "SUM: [".$sum."]\n";
exit;

sub addSubSequence($$) {
  my ($sKey, $levelToAdd) = @_;

  my $curLevel = $levelToAdd - 1;
  my $sequence = $seqStore->{$sKey}->{$curLevel};
  my $seqLen = scalar @{$sequence};

  my @tempStore;
  foreach my $i (1..$seqLen-1) {
    my $i1 = $seqStore->{$sKey}->{$curLevel}[$i-1];
    my $i2 = $seqStore->{$sKey}->{$curLevel}[$i];
    my $delta = $i2-$i1;
    push(@tempStore,$delta);
  }
  $seqStore->{$sKey}->{$levelToAdd} = \@tempStore;
}

sub isZeroed($$) {
  my ($sKey, $level) = @_;

  my $res = 1;
  my @data = @{$seqStore->{$sKey}->{$level}};

  foreach my $i (@data) {
    if ($i != 0) {
      $res = 0;
      last;
    }
  }
  return $res;
}

sub dumpa($) {
  my ($store) = @_;

  foreach my $sKey (sort keys %{$store}) {
    my @subs = sort keys (%{$store->{$sKey}});
    printf("Main [%s] => subs: [%s]\n", $sKey, join('|', @subs));
    foreach my $s (@subs) {
      printf("\t[%s]: [%s]\n", $s, join('|', @{$store->{$sKey}->{$s}}));  
    }
  }
}