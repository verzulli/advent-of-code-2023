TL/DR;
======

You've array of numbers, on top of which you've to apply some calculation.
In P1, those calculation end in a new value added at the end... and you need to sum them.
In P2, similar calculation end in a new value added at the beginning... and you need to sum them.

Original text
=============

*to be written. Sorry!*

My notes
========

IMO, the worth-noting point is the wide use of nested HASHes to keep track of the whole structure and the related code to travel all around.

Nothing special... as far as you have a clear idea about them.

As for P2, I opted for a single PERL for both P1 and P2, with P2 requiring a `P2` param at the end

Solutions
=========
As for my solution:

* My P1 input data are in the "input.txt"
* My implemented solution is [solve_day9.pl](solve_day9.pl)
* P2 result can be obtained by adding `P2` as argument, at the end of the command

Results
=======

P1
--

    [verzulli@XPSGarr DAY_09]$ perl solve_day9.pl | head -10
    Working with SEQ [1] of size [21]: [14|23|30|32|26|9|-22|-70|-138|-229|-346|-492|-670|-883|-1134|-1426|-1762|-2145|-2578|-3064|-3606]
    Working with SEQ [1] of size [20]: [9|7|2|-6|-17|-31|-48|-68|-91|-117|-146|-178|-213|-251|-292|-336|-383|-433|-486|-542]
    Working with SEQ [1] of size [19]: [-2|-5|-8|-11|-14|-17|-20|-23|-26|-29|-32|-35|-38|-41|-44|-47|-50|-53|-56]
    [...]
        Serie [91]: last [17863025]
        Serie [92]: last [21685763]
        Serie [93]: last [2311]
        Serie [94]: last [-773337]
        Serie [95]: last [791885]
        Serie [96]: last [28431735]
        Serie [97]: last [13299704]
        Serie [98]: last [4508]
        Serie [99]: last [681]
    SUM: [1980437560]

P2
--
    [verzulli@XPSGarr DAY_09]$ perl solve_day9.pl P2 | head -n 10
    We are on P2! Let's act accordingly!
    Working with SEQ [1] of size [21]: [14|23|30|32|26|9|-22|-70|-138|-229|-346|-492|-670|-883|-1134|-1426|-1762|-2145|-2578|-3064|-3606]
    Working with SEQ [1] of size [20]: [9|7|2|-6|-17|-31|-48|-68|-91|-117|-146|-178|-213|-251|-292|-336|-383|-433|-486|-542]
    Working with SEQ [1] of size [19]: [-2|-5|-8|-11|-14|-17|-20|-23|-26|-29|-32|-35|-38|-41|-44|-47|-50|-53|-56]
    [...]
        Serie [91]: first [-4]
        Serie [92]: first [4]
        Serie [93]: first [1]
        Serie [94]: first [7]
        Serie [95]: first [6]
        Serie [96]: first [0]
        Serie [97]: first [9]
        Serie [98]: first [9]
        Serie [99]: first [10]
    SUM: [977]
