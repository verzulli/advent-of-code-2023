TL/DR;
======
Given a list of string, one per line, find the first and the last cipher to form a two-cipher number, to sum allover the file.


Original text
=============

    [...]
    The newly-improved calibration document consists of lines of text; each line originally contained a specific calibration value that the Elves now need to recover. On each line, the calibration value can be found by combining the first digit and the last digit (in that order) to form a single two-digit number.

    For example:

    1abc2
    pqr3stu8vwx
    a1b2c3d4e5f
    treb7uchet

    In this example, the calibration values of these four lines are 12, 38, 15, and 77. Adding these together produces 142.

    Consider your entire calibration document. What is the sum of all of the calibration values?

My notes
========
I'm going to implement the solution in [PERL](https://www.perl.org), as it's perfect to read input file and work properly on it.

* My input data are in the "input.txt"
* My implemented solution is "solve_day1.pl"

Solving approach
----------------
Retrieving ciphers is really simple, thanks to RegExps. 

So, the main point of P1 is in lines 42 and 48.

As for Part-two, things are slightly more complex. 

    --- Part Two ---
    Your calculation isn't quite right. It looks like some of the digits are actually spelled out with letters: one, two, three, four, five, six, seven, eight, and nine also count as valid "digits".

    Equipped with this new information, you now need to find the real first and last digit on each line. For example:

    two1nine
    eightwothree
    abcone2threexyz
    xtwone3four
    4nineeightseven2
    zoneight234
    7pqrstsixteen

    In this example, the calibration values are 29, 83, 13, 24, 42, 14, and 76. Adding these together produces 281.

    What is the sum of all of the calibration values?

Replacing strings with ciphers is really easy but... unfortunately there are some catches: "joined" strings.

Namely, the following substrings:

    * twone
    * oneight
    * eightwo

need to be treated differently, based on their placement:

    * if they are THE FIRST match in the string, then the FIRST component need to be selected;
    * if they are THE LAST match in the string, than the SECOND component need to be selected.

It took me while to fully understand this "trick" but, in the end, I was succesfull. Related code is in the *ProcessP2* preparsing routine.

Implementation
==============

Please refer to [`solve_day1.pl`](./solve_day1.pl)

Results
=======
    [verzulli@XPSGarr DAY_01]$ perl solve_day1.pl  | head
    F:[1] - L:[1] - value:[11] - source:[six1mpffbnbnnlxthree                         ]
    F:[4] - L:[2] - value:[42] - source:[4eight3one92                                 ]
    F:[9] - L:[8] - value:[98] - source:[9nine2xnhvjtjlzj48                           ]
    [...]
    F:[7] - L:[4] - value:[74] - source:[74eightrvtconebgjbpnqlslcs                   ]
    F:[1] - L:[1] - value:[11] - source:[nbmntwolntd1zvzplfzthree11seven              ]
    F:[2] - L:[2] - value:[22] - source:[kpzfgpxdonesix2fourninefourfour              ]
    F:[5] - L:[5] - value:[55] - source:[fbdqzbmjnkmqcgeight5five                     ]
    F:[4] - L:[6] - value:[46] - source:[425six14two46                                ]
    F:[8] - L:[8] - value:[88] - source:[jhctmxconelfkgmprnfourseven8twofkjvlvnjgd    ]
    F:[2] - L:[2] - value:[22] - source:[twonrpvnnmvkh2threejzcpz                     ]
    Total: 55090

Results - part TWO
==================
    [verzulli@XPSGarr DAY_01]$ perl solve_day1.pl  | head
    F:[6] - L:[3] - value:[63] - source:[six1mpffbnbnnlxthree                         ]
    F:[4] - L:[2] - value:[42] - source:[4eight3one92                                 ]
    F:[9] - L:[8] - value:[98] - source:[9nine2xnhvjtjlzj48                           ]
    F:[4] - L:[1] - value:[41] - source:[jxrbrt4jmnmlonesznvbjrsn                     ]
    F:[7] - L:[5] - value:[75] - source:[nsvkljgpfn77pvfour5j                         ]
    [...]
    F:[8] - L:[5] - value:[85] - source:[fbdqzbmjnkmqcgeight5five                     ]
    F:[4] - L:[6] - value:[46] - source:[425six14two46                                ]
    F:[1] - L:[2] - value:[12] - source:[jhctmxconelfkgmprnfourseven8twofkjvlvnjgd    ]
    F:[2] - L:[3] - value:[23] - source:[twonrpvnnmvkh2threejzcpz                     ]
    Total: 54845
