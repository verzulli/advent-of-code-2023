#!/usr/bin/perl

# ------------------------------------------------------------------------------
# Copyright 2022, Damiano Verzulli - damiano.verzulli@garrlab.it
#
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU Affero General Public License as published by the 
# Free Software Foundation, either version 3 of the License, or (at your 
# option) any later version.
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
# for more details.
# You should have received a copy of the GNU Affero General Public License 
# along with this program. If not, see <https://www.gnu.org/licenses/>. 
# ------------------------------------------------------------------------------
  
# This is the routine taking care of additional P2 processing
sub ProcessP2($);

open(FH, "./input.txt") || die ("Unable to open input.txt: $!");

# Let's initialize the temporary SUM
my $sum=0;

# Let's read the file, line by line
while (my $line = <FH>) {

  # let's get rid of the terminating newline
  chomp($line);

  # let's keep track of original line, for debugging purposes
  my $originalLine = $line;

  # If we're in P2, we need to add additional parsing/processing.
  # We enclose such preprocessing, in ProcessP2 routine (see below)
  # Note: comment it to get P1 result
  $line = ProcessP2($line);

  # Let's extract the FIRST digit
  my $first;
  if ($line =~ /(\d)\w*$/) {
    $first = $1;
  };

  # Let's extract the LAST digit
  my $last;
  if ($line =~ /^\w*(\d)/) {
    $last = $1;
  };

  # join the digits and update the temporary sum
  my $combined = $first * 10 + $last;
  $sum = $sum + $combined;

  # let's dump some debugging info
  printf("F:[%d] - L:[%d] - value:[%2d] - source:[%-45s]\n",$first,$last,$combined,$originalLine);
}

# here is the total sum...
print "Total: " . $sum . "\n";

exit;

# This routine take care of additional P2 processing
sub ProcessP2($) {

  my ($line) = @_;

  # Warning: INPUT file contains some "joined" terms, like:
  # twone, oneight, eightwo.
  # Those *ARE* problematic, as:
  # - if they are THE FIRST matching in the source string, than
  #   the first element need to be considered (eg: in twone, two)
  # - if they are THE LAST matching in the source string, than
  #   the last element need to be considered (eg: in twone, one)

  # we're NOT considering threeight, fiveight, sevenine as they are
  # not included in our input
  
  # TWOONE
  # if match "at start", substitute the 2
  if ($line =~ /^([a-z]*)twone(.*)$/) {
    $line = $1."2ne".$2;
  }
  # if match "at end", substitute the 1
  if ($line =~ /^(.*)twone([a-z]*)$/) {
    $line = $1."tw1".$2;
  }

  # ONEIGHT
  # if match "at start", choose 1
  if ($line =~ /^([a-z]*)oneight(.*)$/) {
    $line = $1."1ight".$2;
  }
  # if match "at end", choose 8
  if ($line =~ /^(.*)oneight([a-z]*)$/) {
    $line = $1."on8".$2;
  }

  # EIGHTWO
  # if match "at start", choose 8
  if ($line =~ /^([a-z]*)eightwo(.*)$/) {
    $line = $1."8wo".$2;
  }
  # if match "at end", choose 2
  if ($line =~ /^(.*)eightwo([a-z]*)$/) {
    $line = $1."eigh2".$2;
  }

  # now, with above "fix", we can safely substitute strings to ciphers
  $line =~ s/one/1/ig;
  $line =~ s/two/2/ig;
  $line =~ s/three/3/ig;
  $line =~ s/four/4/ig;
  $line =~ s/five/5/ig;
  $line =~ s/six/6/ig;
  $line =~ s/seven/7/ig;
  $line =~ s/eight/8/ig;
  $line =~ s/nine/9/ig;

  # let's return the final $line, to continue elaboration
  # based exactly on P1 logic
  return $line
}