#!/usr/bin/perl

# ------------------------------------------------------------------------------
# Copyright 2022, Damiano Verzulli - damiano.verzulli@garrlab.it
#
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU Affero General Public License as published by the 
# Free Software Foundation, either version 3 of the License, or (at your 
# option) any later version.
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
# for more details.
# You should have received a copy of the GNU Affero General Public License 
# along with this program. If not, see <https://www.gnu.org/licenses/>. 
# ------------------------------------------------------------------------------

# 134 => NO  
open(FH, "./input.txt") || die ("Unable to open input.txt: $!");

# Let's initialize the temporary SUM
my $sum=0;

# Let's read the file, line by line
while (my $line = <FH>) {

  chomp $line;

  my $id; my @sets;

  # Let's exctract the Game ID and the string with the whole set of cubes
  if ($line =~ /^Game\s(\d+):\s(.*)$/) {
    $id = $1;
    my $tmp = $2;

    # let's retrieve the single sets 
    my @t = split(/;/,$2);

    # let's keep track of "current" minimin
    # (that, indeed, is the "maximum" values over the various sets)
    my $r_min=0; my $g_min=0; my $b_min=0;

    # let's loop over each single set
    foreach my $e (@t) {
      my $r=0; my $g=0; my $b=0;

      # update the counters for each value (red, green, blue)
      my @x = split(/,/,$e);
      foreach my $j (@x) {
        if ($j =~ /(\d+)\s*red/) {
          $r += $1;
        } elsif ($j =~ /(\d+)\s*green/) {
          $g += $1;
        } elsif ($j =~ /(\d+)\s*blue/) {
          $b += $1;
        } else {
          print "ALLARME ROSSO con [".$j."]\n";
          exit;
        }
      }

      # update the minimum... if needed
      if ($r > $r_min) {
        $r_min = $r;
      }
      if ($g > $g_min) {
        $g_min = $g;
      }
      if ($b > $b_min) {
        $b_min = $b;
      }
    };

    # calculate the "power"
    my $power = $r_min * $g_min * $b_min;

    printf("[%d] MIN [%d/%d/%d] - power: [%d]\n", $id, $r_min, $g_min, $b_min, $power);

    # update the sum
    $sum += $power;
  } else {
    print "ALLARME ROSSO con la linea [".$line."]\n";
  }
}

print "Sum=[".$sum."]\n";