TL/DR;
======
Given a list of string, one per line, perform various calculation over each single elements of a substring of the line...

Original text
=============

    [...]
    You play several games and record the information from each game (your puzzle input). Each game is listed with its ID number (like the 11 in Game 11: ...) followed by a semicolon-separated list of subsets of cubes that were revealed from the bag (like 3 red, 5 green, 4 blue).

    For example, the record of a few games might look like this:

    Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
    Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
    Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
    Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
    Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green

    In game 1, three sets of cubes are revealed from the bag (and then put back again). The first set is 3 blue cubes and 4 red cubes; the second set is 1 red cube, 2 green cubes, and 6 blue cubes; the third set is only 2 green cubes.

    The Elf would first like to know which games would have been possible if the bag contained only 12 red cubes, 13 green cubes, and 14 blue cubes?

    In the example above, games 1, 2, and 5 would have been possible if the bag had been loaded with that configuration. However, game 3 would have been impossible because at one point the Elf showed you 20 red cubes at once; similarly, game 4 would also have been impossible because the Elf showed you 15 blue cubes at once. If you add up the IDs of the games that would have been possible, you get 8.

    Determine which games would have been possible if the bag had been loaded with only 12 red cubes, 13 green cubes, and 14 blue cubes. What is the sum of the IDs of those games?

My notes
========
I'm going to implement the solution in [PERL](https://www.perl.org), as it's perfect to read input file and work properly on it.

* My input data are in the "input.txt"
* My implemented solution is "solve_day2.pl"

Solving approach
----------------
As usual, exctracting components is very easy, thanks to RegExps.

Unfortunately I initially got the problem **WRONG**, as I perform the "sum" for each game, allover the **whole** sets, while, on the contrary, checking maximum (12r, 13g, 14b) need to be performed for **each single set**. This error costed me more than two hours of debugging :-(

Apart from the above, the code is quite simple and easily understandble, with no particular remark.

P2
--
P2 is even simpler.

    --- Part Two ---

    The Elf says they've stopped producing snow because they aren't getting any water! He isn't sure why the water stopped; however, he can show you how to get to the water source to check it out for yourself. It's just up ahead!

    As you continue your walk, the Elf poses a second question: in each game you played, what is the fewest number of cubes of each color that could have been in the bag to make the game possible?

    Again consider the example games from earlier:

    Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
    Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
    Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
    Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
    Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green

        In game 1, the game could have been played with as few as 4 red, 2 green, and 6 blue cubes. If any color had even one fewer cube, the game would have been impossible.
        Game 2 could have been played with a minimum of 1 red, 3 green, and 4 blue cubes.
        Game 3 must have been played with at least 20 red, 13 green, and 6 blue cubes.
        Game 4 required at least 14 red, 3 green, and 15 blue cubes.
        Game 5 needed no fewer than 6 red, 3 green, and 2 blue cubes in the bag.

    The power of a set of cubes is equal to the numbers of red, green, and blue cubes multiplied together. The power of the minimum set of cubes in game 1 is 48. In games 2-5 it was 12, 1560, 630, and 36, respectively. Adding up these five powers produces the sum 2286.

    For each game, find the minimum set of cubes that must have been present. What is the sum of the power of these sets?

The interesting point is that the "minimum" to be retrieved, is, actually, the "maximum" value calculated over the Games' set...


Implementation
==============

The code should be quite easy to understand. Please refer to:

* [`solve_day2.pl`](./solve_day2.pl) for P1
* [`solve_day2_P2.pl`](./solve_day2_P2.pl) for P2

Results
=======

P1
--
    [verzulli@XPSGarr DAY_02]$ perl solve_day2.pl  | head
    [  1] tuple [ 7 blue, 19 red, 1 green] impossible!
    [  2] tuple [ 16 green, 2 blue] impossible!
    [  3] line [Game 3: 5 red, 9 blue, 1 green; 5 red; 11 red, 2 green, 8 blue; 2 green, 6 blue] => OK!
    [  4] line [Game 4: 2 red, 5 green; 2 blue, 3 red, 3 green; 3 red, 2 blue; 8 green, 2 red] => OK!
    [...]
    [ 97] line [Game 97: 9 red, 5 green, 2 blue; 12 red, 1 blue, 11 green; 7 green, 4 red, 2 blue; 1 blue, 6 red, 10 green] => OK!
    [ 98] line [Game 98: 5 green, 5 red, 11 blue; 1 red, 10 blue, 7 green; 8 red, 1 blue, 7 green; 8 green, 11 red, 2 blue; 4 red, 5 blue, 2 green; 10 green, 5 red, 9 blue] => OK!
    [ 99] tuple [ 6 green, 13 red, 1 blue] impossible!
    [100] tuple [ 15 red, 9 green] impossible!
    Sum=[2162]

P2
--
    [verzulli@XPSGarr DAY_02]$ perl solve_day2_P2.pl | head
    [1] MIN [19/7/8] - power: [1064]
    [2] MIN [6/17/5] - power: [510]
    [3] MIN [11/2/9] - power: [198]
    [...]
    [98] MIN [11/10/11] - power: [1210]
    [99] MIN [13/6/13] - power: [1014]
    [100] MIN [15/13/11] - power: [2145]
    Sum=[72513]
