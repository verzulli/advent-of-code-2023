TL/DR;
======

You need to travel along a path designed by some basic instruction. You need to start from AAA and end to ZZZ and you need to count the steps required.

In P2, you've several startig card, the ones ending with an "A" and several distinct ending card (the ones ending with "Z"). You move with the same rule but... need to check how many steps are required to have **ALL** the starting card, at the same time, reaching an ending card.

**WARNING!!**
I spent some time to see that every starting card, once reached an ending card... if it continue traveling, it goes through **exactly** the same path. This means that every starting card run "a loop", with the very same number of steps.

That means that... you can apply LCM (Lease Common Multiplier) to check the minimun value to have all of them on an endinf card.
** This was not reported in the challenge text!!!! **

Original text
=============

*to be written. Sorry!*

My notes
========

* P1: straightforward
* P2: lots of headaches before checking the LCM issue (see above)

Solutions
=========
As for my solution:

* My P1 and P2 input data are in the "input.txt"
* P1: My implemented solution is [solve_day8.pl](solve_day8.pl)
* P2: My implemented solution is [solve_day8_P2.pl](solve_day8_P2.pl)

Results
=======

P1
--

    [verzulli@XPSGarr DAY_08]$ perl solve_day8.pl | head -n 10
    Load directions: [L|R|R|R|L|R|R|L|L|R|R|L|R|R|L|R|R|L|R|R|L|R|L|L|R|L|R|L|L|R|R|L|R|L|R|R|R|L|R|R|L|R|R|L|L|R|L|R|L|R|L|R|R|R|L|R|R|R|L|L|R|L|R|R|R|L|L|R|R|R|L|R|L|L|R|R|R|L|L|R|R|L|R|L|R|L|R|R|R|L|L|R|R|L|R|R|R|L|L|R|R|L|R|L|R|R|R|L|L|R|R|R|L|R|R|L|R|L|R|R|R|L|L|R|R|L|R|R|R|L|R|R|L|L|R|R|L|R|R|L|R|R|R|L|R|R|R|L|R|R|R|L|R|R|L|R|R|R|L|L|R|L|R|L|R|L|R|R|R|L|R|R|L|R|R|R|L|R|R|L|R|L|R|R|L|R|L|R|R|R|L|R|R|R|L|R|R|L|R|R|R|L|L|R|R|R|L|L|R|R|L|R|L|R|R|R|L|R|L|R|L|R|R|R|L|R|L|R|L|R|L|R|R|L|R|L|R|R|L|R|R|L|L|R|R|R|L|R|L|L|R|R|L|R|R|R|L|R|R|R|L|L|R|R|L|R|L|L|L|L|R|R|L|R|R|R|R]
    Found rule [PGQ] => left: [QRB] - right: [MJB]
    Found rule [JQC] => left: [MNM] - right: [TLQ]
    Found rule [HNP] => left: [NKD] - right: [PJT]
    Found rule [MDM] => left: [SPC] - right: [RJP]
    Found rule [QMZ] => left: [BFS] - right: [TVG]
    [..]
    [verzulli@XPSGarr DAY_08]$ perl solve_day8.pl | tail -n 10
    STEP [13010] - We're in [CJG]: moving [L] to [SXR]
    STEP [13011] - We're in [SXR]: moving [L] to [HPK]
    STEP [13012] - We're in [HPK]: moving [L] to [SNJ]
    STEP [13013] - We're in [SNJ]: moving [R] to [CNN]
    STEP [13014] - We're in [CNN]: moving [R] to [RLQ]
    STEP [13015] - We're in [RLQ]: moving [L] to [PML]
    STEP [13016] - We're in [PML]: moving [R] to [GBP]
    STEP [13017] - We're in [GBP]: moving [R] to [VNN]
    STEP [13018] - We're in [VNN]: moving [R] to [RGK]
    STEP [13019] - We're in [RGK]: moving [R] to [ZZZ]

P2
--

    [verzulli@XPSGarr DAY_08]$ perl solve_day8_P2.pl | head -n 10
    Load directions: [L|R|R|R|L|R|R|L|L|R|R|L|R|R|L|R|R|L|R|R|L|R|L|L|R|L|R|L|L|R|R|L|R|L|R|R|R|L|R|R|L|R|R|L|L|R|L|R|L|R|L|R|R|R|L|R|R|R|L|L|R|L|R|R|R|L|L|R|R|R|L|R|L|L|R|R|R|L|L|R|R|L|R|L|R|L|R|R|R|L|L|R|R|L|R|R|R|L|L|R|R|L|R|L|R|R|R|L|L|R|R|R|L|R|R|L|R|L|R|R|R|L|L|R|R|L|R|R|R|L|R|R|L|L|R|R|L|R|R|L|R|R|R|L|R|R|R|L|R|R|R|L|R|R|L|R|R|R|L|L|R|L|R|L|R|L|R|R|R|L|R|R|L|R|R|R|L|R|R|L|R|L|R|R|L|R|L|R|R|R|L|R|R|R|L|R|R|L|R|R|R|L|L|R|R|R|L|L|R|R|L|R|L|R|R|R|L|R|L|R|L|R|R|R|L|R|L|R|L|R|L|R|R|L|R|L|R|R|L|R|R|L|L|R|R|R|L|R|L|L|R|R|L|R|R|R|L|R|R|R|L|L|R|R|L|R|L|L|L|L|R|R|L|R|R|R|R]
    Found rule [PGQ] => left: [QRB] - right: [MJB]
    Found rule [JQC] => left: [MNM] - right: [TLQ]
    Found rule [HNP] => left: [NKD] - right: [PJT]
    Found rule [MDM] => left: [SPC] - right: [RJP]
    Found rule [QMZ] => left: [BFS] - right: [TVG]
    [...]
    Card: [MXA] => Found [XHZ] at [16343] steps
    Card: [JBA] => Found [FXZ] at [21883] steps
    Card: [VQA] => Found [QMZ] at [11911] steps
    Card: [HSA] => Found [SHZ] at [19667] steps
    Card: [CBA] => Found [KFZ] at [20221] steps
    Card: [AAA] => Found [ZZZ] at [13019] steps
    The L.C.M. of 16343, 21883, 11911, 19667, 20221, 13019 is 13524038372771

