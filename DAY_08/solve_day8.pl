#!/usr/bin/perl

# ------------------------------------------------------------------------------
# Copyright 2022, Damiano Verzulli - damiano.verzulli@garrlab.it
#
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU Affero General Public License as published by the 
# Free Software Foundation, either version 3 of the License, or (at your 
# option) any later version.
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
# for more details.
# You should have received a copy of the GNU Affero General Public License 
# along with this program. If not, see <https://www.gnu.org/licenses/>. 
# ------------------------------------------------------------------------------

use Data::Dumper;
use strict;


my $runInP2 = 0;
if ($ARGV[0] eq 'P2') {
  print "We are on P2! Let's act accordingly!\n";
  $runInP2 = 1;
}

# We need a "store" for all our "rules" (left/right)
my $ruleStore = {};

# Let's loop over the input
open(FH, "./input.txt") || die ("Unable to open input.txt: $!");

# Load the DIRECTION
my $directionString = <FH>;
chomp ($directionString);

my @directions = split(//,$directionString);

print "Load directions: [" . join('|',@directions) . "]\n";

my $dummy = <FH>;

while (my $line = <FH>) {
  chomp($line);

  # retrieve rule
  if ($line =~ /^(\w+)\s*=\s*\((\w+),\s*(\w+)\)$/) {
    my ($code, $left, $right) = ($1, $2, $3);

    # Load RULE
    $ruleStore->{$code}->{'L'} = $left;
    $ruleStore->{$code}->{'R'} = $right;

    printf("Found rule [%s] => left: [%s] - right: [%s]\n", $code, $left, $right);
  } else {
    print "Grande caos con [" . $line . "]\n";
    exit;
  }
}

print "Map LOADED! Let's start traveling...\n";

my $dirPointer=0;
my $stepCounter=1;
my $nextType = $directions[$dirPointer];

# retrieve first L/R destination
my $curCode = 'AAA';
my $nextRuleCode = '';
while ($nextRuleCode ne 'ZZZ') {
  $nextRuleCode = $ruleStore->{$curCode}->{$nextType};
  printf("STEP [%4d] - We're in [%s]: moving [%s] to [%s]\n",$stepCounter, $curCode, $nextType, $nextRuleCode);

  $dirPointer++;
  $stepCounter++;
  if ($dirPointer > $#directions) {
    $dirPointer = 0
  };
  $nextType = $directions[$dirPointer];

  $curCode = $nextRuleCode;
}


