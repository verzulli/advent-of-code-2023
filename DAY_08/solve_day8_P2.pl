#!/usr/bin/perl

# ------------------------------------------------------------------------------
# Copyright 2022, Damiano Verzulli - damiano.verzulli@garrlab.it
#
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU Affero General Public License as published by the 
# Free Software Foundation, either version 3 of the License, or (at your 
# option) any later version.
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
# for more details.
# You should have received a copy of the GNU Affero General Public License 
# along with this program. If not, see <https://www.gnu.org/licenses/>. 
# ------------------------------------------------------------------------------

$|=1;

use Data::Dumper;
use strict;

use lib '.';
use LCM;

sub dumpStartCodes($);

my $runInP2 = 0;
if ($ARGV[0] eq 'P2') {
  print "We are on P2! Let's act accordingly!\n";
  $runInP2 = 1;
}

# We need a "store" for all our "rules" (left/right)
my $ruleStore = {};

# Let's loop over the input
open(FH, "./input.txt") || die ("Unable to open input.txt: $!");

# Load the DIRECTION
my $directionString = <FH>;
chomp ($directionString);

my @directions = split(//,$directionString);

print "Load directions: [" . join('|',@directions) . "]\n";

my $dummy = <FH>;

# to keep track of P2 startCodes
my $startCodes;

my $ns = 0;
while (my $line = <FH>) {
  chomp($line);

  # retrieve rule
  if ($line =~ /^(\w+)\s*=\s*\((\w+),\s*(\w+)\)$/) {
    my ($code, $left, $right) = ($1, $2, $3);

    # Load RULE
    $ruleStore->{$code}->{'L'} = $left;
    $ruleStore->{$code}->{'R'} = $right;

    printf("Found rule [%s] => left: [%s] - right: [%s]\n", $code, $left, $right);

    # ADD P2 logic
    if ($code =~ /A$/) {
      $startCodes->{$code} = {};
      $startCodes->{$code}->{'current'} = $code;
      $startCodes->{$code}->{'next'} = '';
    }
  } else {
    print "Grande caos con [" . $line . "]\n";
    exit;
  }
}

print "Map LOADED! Let's start traveling...\n";

my $dirPointer=0;
my $stepCounter=1;
my $nextType = $directions[$dirPointer];


my @allcode =  keys %{$startCodes};
print "Found [" . scalar @allcode . "] start codes...\n";
my $curCode = shift @allcode;
print "Working with: [" . $curCode . "]\n";


my $prevFind = 0;
my $curFind = 0;

my $loop = 1;

my @result = ();

while ($loop) {

  # Assume we finished (all **Z destination)
  $loop = 0;

  $startCodes->{$curCode}->{'current'} = $startCodes->{$curCode}->{'next'} || $curCode;

  # Are we looking LEFT or RIGHT? Which is our "next" code?
  my $nextRuleCode = $ruleStore->{$startCodes->{$curCode}->{'current'}}->{$nextType};
  $startCodes->{$curCode}->{'next'} = $nextRuleCode;

  # if we are NOT in a "**Z", we need to loop. Let's remember...
  if ($nextRuleCode =~ /Z$/) {
    printf("Card: [%s] => Found [%s] at [%d] steps\n", $curCode, $nextRuleCode, $stepCounter);
    
    push (@result, $stepCounter);

    $stepCounter=0;

    # move forward with START cards...
    $curCode = shift @allcode;
    
    last if (!$curCode);
  }
  $loop = 1;

  $dirPointer++;
  $stepCounter++;
  if ($dirPointer > $#directions) {
      $dirPointer = 0
  };
  $nextType = $directions[$dirPointer];

}

my $lcm = LCM->new(\@result);
my $answer = $lcm->getLCM();
print ("The L.C.M. of ", join(", ", @result), " is $answer\n");

exit;

sub dumpStartCodes($) {

  my $startCodes = shift;

  foreach my $curCode (keys %{$startCodes}) {
    printf("\t(debug) [%s] => curPos:[%s] - nextPos [%s]\n", 
      $curCode, 
      $startCodes->{$curCode}->{'current'},
      $startCodes->{$curCode}->{'next'}
      );
  }
}
