#!/usr/bin/perl

# ------------------------------------------------------------------------------
# Copyright 2022, Damiano Verzulli - damiano.verzulli@garrlab.it
#
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU Affero General Public License as published by the 
# Free Software Foundation, either version 3 of the License, or (at your 
# option) any later version.
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
# for more details.
# You should have received a copy of the GNU Affero General Public License 
# along with this program. If not, see <https://www.gnu.org/licenses/>. 
# ------------------------------------------------------------------------------

use Data::Dumper;
use strict;

sub getHandCode($$);
sub getP1HandScore($);
sub getP2HandScore($);
sub sortStore();

my $runInP2 = 0;
if ($ARGV[0] eq 'P2') {
  print "We are on P2! Let's act accordingly!\n";
  $runInP2 = 1;
}

# We also need a "store" for all our "hands", to easy related "sort"
# and associated bid
# It's going to be loaded with all hands, from input file
my $handStore = {};

# Let's loop over the input
open(FH, "./input_P2.txt") || die ("Unable to open input.txt: $!");
while (my $line = <FH>) {
  chomp($line);

  # retrieve hand and bid
  my ($hand, $bid) = split(/\s+/,$line);

  # Add this hand to our store
  $handStore->{$hand} = {};
  $handStore->{$hand}->{'bid'} = $bid;
  $handStore->{$hand}->{'code'} = getHandCode($runInP2,$hand);

  printf("Checking hand [%s] with bid [%d]...\n", $hand, $bid);

  # let's create a cardMap for this hand
  my $cardMap = {};

  for my $idx (1 .. length $hand) {
    my $card = substr($hand, $idx-1, 1);

    # update occurrences
    # (we don't need to check existance, thanks to PERL)
    $cardMap->{$card}->{'occurrence'}++;
  }

  # update handStore with this hand data
  $handStore->{$hand}->{'card'} = $cardMap;

  # update handStore with the "score" of this hand
  if ($runInP2) {
    $handStore->{$hand}->{'score'} = getP2HandScore($cardMap);
  } else {
    $handStore->{$hand}->{'score'} = getP1HandScore($cardMap);
  }
}

# Everything is ready! We can "sort" and "calculate..."

# Let's dump hands, based on score... and update "final result"
my $finalProduct = 0;
my $counter = 1;
foreach my $h (sort sortStore keys %{$handStore}) {
  printf("Hand: [%s] => code [%s] - score: [%s] - bid: [%d]\n", 
    $h, 
    $handStore->{$h}->{'code'},
    $handStore->{$h}->{'score'},
    $handStore->{$h}->{'bid'});
  $finalProduct = $finalProduct + ($counter * $handStore->{$h}->{'bid'});
  $counter++;
}

print "Final Product is [" . $finalProduct . "]\n";
close FH;

exit;

# To easy hand comparison, we're adapting its code
# to a proper HEX string
sub getHandCode($$) {
  my ($inP2, $hand) = @_;

  if (! $inP2) {
    # We're in P1

    # source: A, K, Q, J, T, 9, 8, 7, 6, 5, 4, 3, or 2
    # dest: 
    # 2-9 => 2-9
    # T => A
    # J => B
    # Q => C
    # K => D
    # A => E

    $hand =~ s/A/E/g;
    $hand =~ s/K/D/g;
    $hand =~ s/Q/C/g;
    $hand =~ s/J/B/g;
    $hand =~ s/T/A/g;
  } else {
    # We're in P2
    
    # source: A, K, Q, T, 9, 8, 7, 6, 5, 4, 3, 2, J
    # dest: 
    # 2-9 => 2-9
    # T => A
    # Q => B
    # K => C
    # A => D
    # J => 1

    $hand =~ s/A/D/g;
    $hand =~ s/K/C/g;
    $hand =~ s/Q/B/g;
    $hand =~ s/J/1/g;
    $hand =~ s/T/A/g;
  }
 
  return $hand;
}

# from one HAND, create a "string" representing the score of 
# such hand (...a string that can be "compared/sorted")
sub getP1HandScore($) {
  my ($map) = @_;

  # prepare for our output...
  my $resString = '';

  # loop over cards, based on occurrence, descending
  # (so 5 come before 4, that come before 3, etc.)
  foreach my $k (sort { $map->{$b}->{'occurrence'} <=> $map->{$a}->{'occurrence'}} keys %{$map}) {
    $resString = $resString . $map->{$k}->{'occurrence'};
  }

  # fill the rest of the string with a "." (that has a lower ascii val)
  for my $idx (length $resString .. 5) {
    $resString = $resString . '.';
  }

  return $resString;
}

# from one HAND, create a "string" representing the score of 
# such hand (see related P1 func) *BUT* taking care of "J-olly"
sub getP2HandScore($) {
  my ($map) = @_;

  # prepare for our output...
  my $resString = '';

  # Let's see if we have "J"...
  my $numJ = $map->{'J'}->{'occurrence'};

  # remove 'J' from MAP
  delete $map->{'J'};

  # if we have an empty map, than it had 5 Js
  if (scalar keys %{$map} == 0) {
    $resString = '5....';
  } else {
    # loop over cards, based on occurrence, descending
    # (so 5 come before 4, that come before 3, etc.)

    # ...and keep track of "first" element, that we need to increase with Jollies
    my $atStart = 1;
    foreach my $k (sort { $map->{$b}->{'occurrence'} <=> $map->{$a}->{'occurrence'}} keys %{$map}) {

      my $valueToAdd = $map->{$k}->{'occurrence'};
      if ($atStart) {
        if ($numJ > 0) {
          $valueToAdd = $valueToAdd + $numJ;
        }
      }
      $resString = $resString . $valueToAdd;
      $atStart = 0;
    }

    # fill the rest of the string with a "." (that has a lower ascii val)
    for my $idx (length $resString .. 5) {
      $resString = $resString . '.';
    }
  }

  return $resString;  
}

sub sortStore() {
  if ($handStore->{$a}->{'score'} gt $handStore->{$b}->{'score'}) {
    # $a is scored HIGHER than $b
    return 1
  } elsif ($handStore->{$a}->{'score'} lt $handStore->{$b}->{'score'}) {
    # $a is scored LOWER than $b
    return -1
  } else {
    # $a and $b have SAME score. Need to compare "code"
    return ($handStore->{$a}->{'code'} cmp $handStore->{$b}->{'code'})
  }
}