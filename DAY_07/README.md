TL/DR;
======

You're playing sort-of poker-like card game. While playing, you get "scored" based on some rule (similar to poker-ones). You need to calculate the score of your hands, and sort them, from worst one, to best one. If two hands have same score, you've some "rule" for comparison.

Then you need to do some calculation on such an ordered list

In P2 "Jollyes" need to be taken into account, to maximize the hand score...

Original text
=============

*to be written. Sorry!*

My notes
========

Nothing much relevant, with the exception of:

* I performed some CARD-code replacement, to easy the "sorting" algorithm with a lessicographic approach;
* a wonderful application of PERL `sort` function, to sort the result without any hassle;
* a bit tricky situation (an hand with only Jollies) to consider
* I opted for a single PERL for both P1 and P2, with P2 requiring a `P2` param at the end


Solutions
=========
As for my solution:

* My P1 input data are in the "input.txt"
* My implemented solution is [solve_day7.pl](solve_day7.pl)
* My P2 input data are in the "input_P2.txt"
* P2 result can be obtained by adding `P2` as argument, at the end of the command

Results
=======

P1
--

    [verzulli@XPSGarr DAY_07]$ perl solve_day7.pl
    Checking hand [32555] with bid [626]...
    Checking hand [4558J] with bid [55]...
    Checking hand [62K22] with bid [775]...
    Checking hand [T7JJT] with bid [530]...
    Checking hand [K4333] with bid [992]...
    Checking hand [4T884] with bid [682]...
    Checking hand [42222] with bid [694]...
    Checking hand [22333] with bid [122]...
    [...]
    Checking hand [A33J3] with bid [675]...
    Checking hand [5T694] with bid [609]...
    Hand: [23KAT] => code [23DEA] - score: [11111.] - bid: [418]
    Hand: [246AQ] => code [246EC] - score: [11111.] - bid: [69]
    Hand: [25K43] => code [25D43] - score: [11111.] - bid: [105]
    Hand: [25K63] => code [25D63] - score: [11111.] - bid: [382]
    Hand: [264KA] => code [264DE] - score: [11111.] - bid: [475]
    Hand: [27TA5] => code [27AE5] - score: [11111.] - bid: [74]
    Hand: [27KQ4] => code [27DC4] - score: [11111.] - bid: [886]
    Hand: [285TQ] => code [285AC] - score: [11111.] - bid: [488]
    Hand: [29TJQ] => code [29ABC] - score: [11111.] - bid: [605]
    Hand: [29Q85] => code [29C85] - score: [11111.] - bid: [982]
    Hand: [29K63] => code [29D63] - score: [11111.] - bid: [828]
    Hand: [2J798] => code [2B798] - score: [11111.] - bid: [963]
    [...]
    Hand: [AA5AA] => code [EE5EE] - score: [41....] - bid: [178]
    Hand: [AA8AA] => code [EE8EE] - score: [41....] - bid: [818]
    Hand: [AAJAA] => code [EEBEE] - score: [41....] - bid: [88]
    Hand: [AAAKA] => code [EEEDE] - score: [41....] - bid: [766]
    Hand: [AAAA2] => code [EEEE2] - score: [41....] - bid: [807]
    Hand: [AAAAT] => code [EEEEA] - score: [41....] - bid: [535]
    Hand: [AAAAQ] => code [EEEEC] - score: [41....] - bid: [705]
    Hand: [JJJJJ] => code [BBBBB] - score: [5.....] - bid: [735]
    Final Product is [253205868]

P2
--

    [verzulli@XPSGarr DAY_07]$ perl solve_day7.pl P2
    We are on P2! Let's act accordingly!
    Checking hand [32555] with bid [626]...
    Checking hand [4558J] with bid [55]...
    Checking hand [62K22] with bid [775]...
    Checking hand [T7JJT] with bid [530]...
    Checking hand [K4333] with bid [992]...
    Checking hand [4T884] with bid [682]...
    [...]
    Checking hand [A33J3] with bid [675]...
    Checking hand [5T694] with bid [609]...
    Hand: [23KAT] => code [23CDA] - score: [11111.] - bid: [418]
    Hand: [246AQ] => code [246DB] - score: [11111.] - bid: [69]
    Hand: [25K43] => code [25C43] - score: [11111.] - bid: [105]
    Hand: [25K63] => code [25C63] - score: [11111.] - bid: [382]
    Hand: [264KA] => code [264CD] - score: [11111.] - bid: [475]
    Hand: [27TA5] => code [27AD5] - score: [11111.] - bid: [74]
    Hand: [27KQ4] => code [27CB4] - score: [11111.] - bid: [886]
    [...]
    Hand: [7777J] => code [77771] - score: [5.....] - bid: [299]
    Hand: [88JJ8] => code [88118] - score: [5.....] - bid: [147]
    Hand: [9J999] => code [91999] - score: [5.....] - bid: [809]
    Hand: [999JJ] => code [99911] - score: [5.....] - bid: [424]
    Hand: [TTTJT] => code [AAA1A] - score: [5.....] - bid: [514]
    Hand: [QJQQJ] => code [B1BB1] - score: [5.....] - bid: [448]
    Hand: [QJQQQ] => code [B1BBB] - score: [5.....] - bid: [620]
    Hand: [KKJKK] => code [CC1CC] - score: [5.....] - bid: [399]
    Hand: [AAJAA] => code [DD1DD] - score: [5.....] - bid: [88]
    Final Product is [253907829]
