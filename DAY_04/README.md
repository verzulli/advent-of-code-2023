TL/DR;
======

*to be writter. Sorry!*

Original text
=============

*to be written. Sorry!*

My notes
========
Nothing really relevant, except that:

    * I tried to split only the right part (my cards), but I had problems matching (substring search) with the whole left string [eg: single-digit numbers];
    * ...so I splitted also the left part and loop for checking (array grep, with PERL).
    
    As for P2... no particular difficulties.

* My input data are in the "input.txt"
* My implemented solution is "solve_day4.pl"

Implementation
==============

* [`solve_day4.pl`](./solve_day4.pl) for both P1 and P2

Results
=======

*to be written. Sorry!*
