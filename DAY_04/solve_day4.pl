#!/usr/bin/perl

# ------------------------------------------------------------------------------
# Copyright 2022, Damiano Verzulli - damiano.verzulli@garrlab.it
#
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU Affero General Public License as published by the 
# Free Software Foundation, either version 3 of the License, or (at your 
# option) any later version.
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
# for more details.
# You should have received a copy of the GNU Affero General Public License 
# along with this program. If not, see <https://www.gnu.org/licenses/>. 
# ------------------------------------------------------------------------------

use Data::Dumper;

open(FH, "./input.txt") || die ("Unable to open input.txt: $!");
my @lines = <FH>;
close FH;

my $sum=0;
my $cardMap = {};

# [P2] initialize Card counter
foreach my $card (@lines) {
  chomp $card;
  if ($card =~ /^Card\s+(\d+)/) {
    my $idx = $1;
    $cardMap->{$idx}=1;
  }
}

foreach my $card (@lines) {
  chomp $card;
  if ($card =~ /^Card\s+(\d+):\s+(.*)\s\|\s+(.*)$/) {
    my ($idx, $win, $mine) = ($1, $2, $3);

    my @mySet = split(/\s+/,$mine);
    my @cardSet = split(/\s+/,$win);
    printf("[%3d] [%s][%s] => ",$idx, join(';',@cardSet), join(';',@mySet));
    my $count=0;
    foreach my $s (@mySet) {
      my $numMatch = grep $_ eq $s, @cardSet;
      if ($numMatch > 0) {
        $count++;
      }
    }

    # Update P2Map, based on $count
    my $first = $idx + 1;
    next if ($first > scalar(@lines));
    
    my $last = $idx + $count;
    if ($last > scalar(@lines)) {$last = scalar(@lines)};

    printf("[%d] => updating from [%d] to [%d] ---> [%d] times\n",$count, $first, $last, $cardMap->{$idx});
    foreach $l ($first .. $last) {
      $cardMap->{$l} = $cardMap->{$l}+ (1 * $cardMap->{$idx});
    }

    print "\t";
    foreach my $k (sort (keys %{$cardMap})) {
      my $card = $cardMap->{$k};
      printf("[%3d]", $card);
    }
    print "\n";
  }
}


print "\n";
foreach my $k (sort (keys %{$cardMap})) {
  my $card = $cardMap->{$k};
  printf("[%3d]", $card);
  $sum += $card;
}
print "\n";
print "Sum: [" . $sum . "]\n";